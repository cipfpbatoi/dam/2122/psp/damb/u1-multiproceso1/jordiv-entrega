package es.vidal;

import java.io.*;

public class Ejercicio2 {
    final static String RANDOM_FILE_NAME = "randoms.txt";

    public static void main(String[] args) throws IOException, InterruptedException {
        final String random10JarPath = "out/artifacts/Ejercicio2Random10_jar/Ejercicio2Random10.jar";

        Process process = new ProcessBuilder("java", "-jar", random10JarPath).start();

        try {

            String line;
            boolean goOn;
            do {
                System.out.print("Escribe una cadena de carácteres para recibir el número aleatorio del 1 al 10 " +
                        "(Escribe 'Stop' para finalizar el programa): ");
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(process.getOutputStream());
                InputStreamReader inputStreamReader = new InputStreamReader(process.getInputStream());
                BufferedReader bufReader = new BufferedReader(inputStreamReader);
                FileWriter fw = new FileWriter(RANDOM_FILE_NAME);

                /**
                 * Pot utilizar-se la Classe Scanner, tal com he explicat a Classe
                 */
                BufferedReader bufReadCommLine = new BufferedReader(new InputStreamReader(System.in));
                BufferedWriter bufWriter = new BufferedWriter(outputStreamWriter);
                line = bufReadCommLine.readLine();

                bufWriter.write(line);
                bufWriter.newLine();
                bufWriter.flush();

                goOn = !line.equalsIgnoreCase("stop");
                if (goOn) {
                    String number = bufReader.readLine();
                    System.out.println(number);
                    fw.write(number + System.lineSeparator());
                }
            } while (goOn);
        } finally {
            int exitValue = process.waitFor();
            System.out.println(System.lineSeparator() + "Valor de salida: " + exitValue);
            System.out.println(System.lineSeparator() + "El número se ha guardado en el fichero: " + RANDOM_FILE_NAME);
            if (exitValue != 0) {
                System.err.println("ERROR RANDOM10:");
                printChildError(process.getErrorStream());
            }
        }
    }

    /***
     * Este metodo recibe un proceso y lee el errorStream para seguidamente leerlo por pantalla
     * @param errorStream es el ErrorStream del proceso hijo
     */
    private static void printChildError(InputStream errorStream) {
        String line;
        // try ( Scanner sc = new Scanner(errorStream)) {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(errorStream))){
            //while( sc.hasNext()){
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }
}
