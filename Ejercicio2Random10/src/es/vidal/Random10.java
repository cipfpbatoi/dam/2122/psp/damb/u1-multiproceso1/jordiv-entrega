package es.vidal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class Random10 {
    public static void main(String[] args) {

        final Random RANDOM = new Random();
        final int NUMERO_MAXIMO = 11;
        // try ( Scanner sc = new Scanner(System.in)){
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {

            String cadena;

            boolean isGoing = true;
            while (isGoing && (cadena = bufferedReader.readLine()) != null) {
                if (cadena.equalsIgnoreCase("stop")) {
                    isGoing = false;
                } else {
                    System.out.println(RANDOM.nextInt(NUMERO_MAXIMO));
                }
            }
        }catch (IOException e){
            System.err.println(e.getMessage());
        }
    }
}
