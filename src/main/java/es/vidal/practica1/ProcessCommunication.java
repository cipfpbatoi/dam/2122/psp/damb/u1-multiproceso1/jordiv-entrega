package es.vidal.practica1;

import java.io.*;
import java.util.Arrays;

public class ProcessCommunication {

    public static void main(String[] args) throws IOException {

        Process process = new ProcessBuilder(args).start();
        InputStream inStream = process.getInputStream();

        InputStreamReader inStreamReader = new InputStreamReader(inStream);
        BufferedReader bufReader = new BufferedReader(inStreamReader);

        System.out.println("Output for the child process"
                + Arrays.toString(args) + " : ");

        String line;
        while ((line = bufReader.readLine()) != null){
            System.out.println(line);
        }
    }
}
