package es.vidal.practica1;

import java.io.IOException;

/**
 * Hello world!
 *
 */
public class ProccesCreating {
    public static void main( String[] args ) {

        if (args.length <= 0){
            System.err.println("I need a command/program to execute!");
            System.exit(-1);
        }

        Runtime runtime = Runtime.getRuntime();

        try {
            Process process = runtime.exec(args);
            process.destroy();
        }catch (IOException exception){
            System.err.println("IO Exception !!");
            System.exit(-1);
        }
    }
}
