package es.vidal;

import java.io.*;

public class Ejercicio3 {
    public static void main(String[] args) {
        final String minusculasJarPath = "out/artifacts/Ejercicio3Minusculas_jar/Ejercicio3Minusculas.jar";

        Process process = null;
        try {
            process = new ProcessBuilder("java", "-jar", minusculasJarPath).start();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Ací si utilitzes Scanner, es queda prou mes simple el bloc del try

        try (OutputStreamWriter outputStreamWriter = new OutputStreamWriter(process.getOutputStream());
             InputStreamReader inputStreamReader = new InputStreamReader(process.getInputStream());
             BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
             BufferedReader bufferedReader = new BufferedReader(inputStreamReader)) {
            BufferedReader buffReadCommLine = new BufferedReader(new InputStreamReader(System.in));


            String line;
            boolean goOn;
            do {
                System.out.print("Escribe una cadena en letras Mayúsculas que quiera pasar a minúsculas " +
                        "(Escribe 'Finalizar' para detener el programa): ");
                line = buffReadCommLine.readLine();

                bufferedWriter.write(line);
                bufferedWriter.newLine();
                bufferedWriter.flush();

                goOn = !line.equalsIgnoreCase("finalizar");
                if (goOn) {
                    System.out.println(bufferedReader.readLine());
                }
            } while (goOn);
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }finally {
            int exitValue = 0;
            try {
                exitValue = process.waitFor();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(System.lineSeparator() + "La ejecución del programa Minusculas devuelve el valor de salida: " + exitValue);
            if (exitValue != 0) {
                System.err.println("ERROR MINUSCULAS:");
                printChildError(process.getErrorStream());
            }
        }
    }

    /***
     * Este metodo recibe un proceso y lee el errorStream del mismo que recoge el error producido
     * Luego lo muestra por pantalla
     * @param errorStream es el ErrorStream del proceso hijo
     */
    private static void printChildError(InputStream errorStream) {

        String line;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(errorStream))){
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }
}

